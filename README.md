And i have managed to build a working apk (signed with debug key)! 

[Download](https://gitlab.com/peterge/combined-assets/-/raw/main/Combined!.apk)

[![Flathub](https://blog.peterge.de/content/images/2021/12/image-3.png)](https://flathub.org/apps/details/de.peterge.combined)

[Assets are kept in this repo](https://gitlab.com/peterge/combined-assets)

This is the first working release of my small game.

The redesigned ui currently looks like this:
(And might be the final design)

![GAME_UI](https://gitlab.com/peterge/combined-assets/-/raw/main/1628805181.png)

How looked before:

![GAME_UI](https://gitlab.com/peterge/combined-assets/-/raw/main/1628689105.png)


