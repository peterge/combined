extends Node2D

export (int) var number;

export (bool) var scored;
export (bool) var matched;
export (bool) var matched_root;

var move_tween

# Called when the node enters the scene tree for the first time.
func _ready():
	move_tween = get_node("move_tween")
	pass # Replace with function body.

func move(target):
	move_tween.interpolate_property(self, "position", position, target, .4, Tween.TRANS_QUART, Tween.EASE_OUT)
	move_tween.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.4, Tween.TRANS_QUART, Tween.EASE_OUT)
	move_tween.start()

func fadein():
	move_tween.interpolate_property(self, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.4, Tween.TRANS_QUART, Tween.EASE_IN)
	move_tween.start()

func clean():
	get_node("clean/clean2").play("clean")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func dim():
	if not matched_root:
		var sprite = get_node("Sprite")
		sprite.modulate = Color(1, 1, 1, .5);
