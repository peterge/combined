extends Node2D

# Todo List:
# game is basically ready :D
# new tile spawn chance -> irrelevant

#Set min and max of tiles to spawn
#Set them one lower than number to spawn, because of array positions
export (int) var min_number = 0;
export (int) var max_number = 3;

# Grid Variables
export (int) var width;
export (int) var height;
export (int) var spawn_ui_height;
export (int) var x_start;
export (int) var y_start;
export (int) var offset;

# Touch Variables
var controlling = false;
var touch_start_pos

# Graphics
var possible_pieces = [
	preload("res://scenes/1_piece.tscn"),
	preload("res://scenes/2_piece.tscn"),
	preload("res://scenes/3_piece.tscn"),
	preload("res://scenes/4_piece.tscn"),
	preload("res://scenes/5_piece.tscn"),
	preload("res://scenes/6_piece.tscn"),
	preload("res://scenes/7_piece.tscn")
];

var animated_clean = preload("res://scenes/animation_clean.tscn")

# Arrays
var all_pieces = [];
var spawn_piece_array = [];

# Other Variables
#Score
export (String) var highscore = "0";
export (String) var score = "0";

var new_piece_length = 0
var new_piece_orientation = 0

# Was the intro played yet
var intro_played = false;
var enable_game = true;

func make_2d_array():
	var array = [];
	for i in width:
		array.append([]);
		for j in height:
			array[i].append(null);
	return array;

func spawn_pieces():
	for i in width:
		for j in height - 2:
			var rand = floor(rand_range(0, possible_pieces.size()));
			var piece = possible_pieces[rand].instance();
			add_child(piece);
			piece.position = grid_to_pixel(i, j);
			all_pieces[i][j] = piece;

func _ready():
	#Calculate if height > 720 new position for spawn ui. Else take get_viewport_rect().size.y - 240
	if get_viewport_rect().size.y > 1280:
		spawn_ui_height = 1350
	else:
		spawn_ui_height = 1280
	# Place bottom_ui spawn are
	get_parent().get_node("bottom_ui").set_position(Vector2((get_viewport_rect().size.x) / 2 - 120, spawn_ui_height - 240 ))
	# Load highscore & set score to 0
	load_score()
	# Load Intro played variable
	load_intro_played()
	
	if !intro_played:
		# Show intro one time
		get_parent().get_node("intro").visible = true
		get_parent().get_node("intro/intro_button").visible = true
		get_parent().get_node("pause_button").visible = false
		get_parent().get_node("background_dark").visible = true
		controlling = false
	
	get_parent().get_node("highscore_number").bbcode_text = str(highscore)
	get_parent().get_node("score_number").bbcode_text = str(0)
	
	randomize();
	all_pieces = make_2d_array()
	spawn_piece_array = spawn_new_tile(0)
	pass # Replace with function body.

func _process(delta):
	touch_input()
	if get_parent().get_node("intro").visible:
		controlling = false
	if get_parent().get_node("pause_menu").visible:
		controlling = false
	if controlling:
		if enable_game:
			if new_piece_length > 1:
				# Dual Tile
				if new_piece_orientation == 1:
					# Vertical
					spawn_piece_array[0].position = Vector2(get_global_mouse_position().x, get_global_mouse_position().y - 60);
					spawn_piece_array[1].position = Vector2(get_global_mouse_position().x, get_global_mouse_position().y + 60);
				elif new_piece_orientation == 2:
					# Horizontal
					spawn_piece_array[0].position = Vector2(get_global_mouse_position().x - 60, get_global_mouse_position().y);
					spawn_piece_array[1].position = Vector2(get_global_mouse_position().x + 60, get_global_mouse_position().y);
			else:
				spawn_piece_array.position = get_global_mouse_position()

func make_effect(effect, column, row):
	var current = effect.instance()
	current.position = grid_to_pixel(column, row)
	current.pixel_position = grid_to_pixel(column, row)
	add_child(current)

func input_in_spawn_array(y, x):
	# Calculates if the give pixel coordinates of a touch input are inside the new piece array
	if new_piece_length > 1:
		# Dual Tile
		if new_piece_orientation == 1:
			# Vertical
			if y >= spawn_piece_array[0].position.y - offset / 2 && y <= spawn_piece_array[1].position.y + offset / 2:
				if x >= spawn_piece_array[0].position.x - offset / 2 && x <= spawn_piece_array[0].position.x + offset / 2:
					return true
		elif new_piece_orientation == 2:
			# Horizontal
			if y >= spawn_piece_array[0].position.y - offset / 2 && y <= spawn_piece_array[0].position.y + offset / 2:
				if x >= spawn_piece_array[0].position.x - offset / 2 && x <= spawn_piece_array[1].position.x + offset / 2:
					return true
	else:
		# Single Tile
		if y >= spawn_piece_array.position.y - offset / 2 && y <= spawn_piece_array.position.y + offset / 2:
			if x >= spawn_piece_array.position.x - offset / 2 && x <= spawn_piece_array.position.x + offset / 2:
					return true
	return false

func reset_spawn_piece_position():
	# This function resets the position of the spawn piece array to its initial position
	if new_piece_length > 1:
		# Dual Tile
		if new_piece_orientation == 1:
			# Vertical
			spawn_piece_array[0].position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height - 120 - 60);
			spawn_piece_array[1].position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height - 120 + 60);
		elif new_piece_orientation == 2:
			# Horizontal
			spawn_piece_array[0].position = Vector2((get_viewport_rect().size.x) / 2 - 60, spawn_ui_height - 120 );
			spawn_piece_array[1].position = Vector2((get_viewport_rect().size.x) / 2 + 60, spawn_ui_height - 120 );
	else:
		spawn_piece_array.position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height - 120 );
	#print("Reset piece to spawn")

func rotate_spawn_piece():
	if !get_parent().get_node("pause_menu").visible:
		if !get_parent().get_node("intro").visible:
			if controlling:
				if enable_game:
					if new_piece_length > 1:
						if new_piece_orientation == 1:
							# Vertical
							new_piece_orientation = 2
							var tmp_piece = spawn_piece_array[0]
							var tmp_piece_position = spawn_piece_array[0].position
							spawn_piece_array[0] = spawn_piece_array[1]
							spawn_piece_array[0].position = spawn_piece_array[1].position
							spawn_piece_array[1] = tmp_piece
							spawn_piece_array[1].position = tmp_piece_position
						elif new_piece_orientation == 2:
							# Horizontal
							new_piece_orientation = 1
					else:
						# Nothing to do with single piece
						pass
					reset_spawn_piece_position()

func detect_drag(touch_end_pos):
	# This function detects if the gesture is a tap on the spawn piece or if it is dragged further than offset / 4
	if !get_parent().get_node("pause_menu").visible:
		if controlling:
			var minimum_drag = offset / 4
			if touch_start_pos == null: 
				return
			var drag = touch_end_pos - touch_start_pos
			if abs(drag.x) > minimum_drag or abs(drag.y) > minimum_drag:
				return true
			else:
				return false

func touch_input():
	if !get_parent().get_node("pause_menu").visible:
		if Input.is_action_just_pressed("ui_touch"):
			# Check if touch position is inside the spawn array
			if input_in_spawn_array(get_global_mouse_position().y, get_global_mouse_position().x):
				# Allow controlling if touch position is in spawn array
				controlling = true
				# Register touch start position which is needed to calculate if gesture is a tap or drag
				touch_start_pos = get_global_mouse_position()
	if !get_parent().get_node("pause_menu").visible:
		if Input.is_action_just_released("ui_touch"):
			if detect_drag(get_global_mouse_position()):
				pass
			else:
				if !get_parent().get_node("intro").visible:
					if !get_parent().get_node("pause_menu").visible:
						rotate_spawn_piece()
			
			if !get_parent().get_node("pause_menu").visible:
				if is_in_grid(vector_to_grid(get_global_mouse_position())) && controlling:
					# Variable which containe positive grid position where the tile is released
					var released_grid_position = Vector2(abs(pixel_to_grid(get_global_mouse_position().x, get_global_mouse_position().y).x), abs(pixel_to_grid(get_global_mouse_position().x, get_global_mouse_position().y).y));
					# Check if drop position piece number equals 0
					if !is_instance_valid(all_pieces[released_grid_position.x][released_grid_position.y]):
						controlling = false
						if new_piece_length > 1:
							#if new_piece_orientation == 1:
								# Vertical + Horizontal
								#print(pixel_to_grid(spawn_piece_array[0].position.x, spawn_piece_array[0].position.y))
								for i in spawn_piece_array.size():
									# Append dragging piece to array
									all_pieces[pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).x][pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).y] = spawn_piece_array[i];
									# Set precise position of dragged piece
									spawn_piece_array[i].position = grid_to_pixel(pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).x, pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).y)
									#print(all_pieces[pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).x][pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).y].number)
									all_pieces[pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).x][pixel_to_grid(spawn_piece_array[i].position.x, spawn_piece_array[i].position.y).y].matched_root = true
						else:
							# Single
							# Append dragging piece to array
							all_pieces[released_grid_position.x][released_grid_position.y] = spawn_piece_array;
							# Set precise position of dragged piece
							spawn_piece_array.position = grid_to_pixel(released_grid_position.x, released_grid_position.y);
							all_pieces[released_grid_position.x][released_grid_position.y].matched_root = true;
						
						var match_found = true
						
						while match_found:
							match_found = find_matches()
							# Handle the rare condition that both dual pieces becomes a root in different matches
							if match_found:
								for i in width:
									for j in height:
										if is_instance_valid(all_pieces[i][j]):
											if not all_pieces[i][j].matched && all_pieces[i][j].matched_root:
												all_pieces[i][j].matched_root = false
							destroy_matched()
							if is_instance_valid(all_pieces[released_grid_position.x][released_grid_position.y]):
								is_instance_valid(all_pieces[released_grid_position.x][released_grid_position.y].matched);
							if match_found:
								yield(get_tree().create_timer(0.75), "timeout")
							# Update Score
							get_parent().get_node("score_number").bbcode_text = String(score)
							save_score()
						for i in width:
							for j in height:
								if is_instance_valid(all_pieces[i][j]):
									all_pieces[i][j].matched_root = false;
							
						var available_piece_length = array_space_left(all_pieces)
						if available_piece_length == 0:
							reset_game()
							game_over()
							return
						elif available_piece_length == 2:
							spawn_piece_array = spawn_new_tile(0)
						else:
							spawn_piece_array = spawn_new_tile(available_piece_length)
							
					# Else reset tile because the field is already in use
					else:
						#print("There is a piece in this Grid position!")
						controlling = false
						reset_spawn_piece_position()
				else:
					# Not inside the grid!
					controlling = false
					reset_spawn_piece_position()

func print_matched_root():
	for i in range(height - 1, -1, -1):
		var row = ""
		for j in width:
			if all_pieces[j][i] != null:
				if all_pieces[j][i].matched_root:
					row = row + "y"
				else:
					row = row + "n"
			else:
				row = row + "-"

func print_all_numbers():
	for i in range(height - 1, -1, -1):
		var row = ""
		for j in width:
			if all_pieces[j][i] != null:
				row = row + String(all_pieces[j][i].number)
			else:
				row = row + "0"

func array_is_space_left(array):
	for i in width:
		for j in height:
			if array[i][j] == null:
				return true
	return false

func array_space_left(array):
	# Calculate how long the next piece can be to fit in the grid
	var tile_left = 0
	for i in width:
		for j in height:
			if !is_instance_valid(array[i][j]):
				if i > 0 && i < width - 1:
					if !is_instance_valid(array[i + 1][j]) or !is_instance_valid(array[i - 1][j]):
						tile_left = 2
						return tile_left
					else: 
						tile_left = 1
						return tile_left
				elif j > 0 && j < height - 1:
					if !is_instance_valid(array[i][j + 1]) or !is_instance_valid(array[i][j - 1]):
						tile_left = 2
						return tile_left
					else: 
						tile_left = 1
						return tile_left
	return tile_left

func game_over():
	print("Game Over - Resetting")
	get_tree().change_scene("res://ui/game_over_screen.tscn")

func reset_game():
	for i in width:
		for j in height:
			if is_instance_valid(all_pieces[i][j]):
				all_pieces[i][j].queue_free()
	all_pieces = make_2d_array();
	spawn_piece_array = spawn_new_tile(0)
	score = 0
	# Update Score
	get_parent().get_node("score_number").bbcode_text = String(score)
	return true

func spawn_new_tile(piece_length):
	# Spawn min - max
	var rand = floor(rand_range(min_number, max_number));
	
	var array = [];
	var rand_new_piece_length
	var rand_new_piece_orientation = floor(rand_range(0, 2));
	
	if piece_length == 1:
		rand_new_piece_length = 1
	elif piece_length == 2:
		rand_new_piece_length = 2
	elif piece_length == 0:
		rand_new_piece_length = floor(rand_range(1, 3));
	
	new_piece_orientation = 0
	new_piece_length = 0
	if rand_new_piece_length == 2:
		# Spawn Dual Tile
		new_piece_length = 2
		# Dont spawn the same pieces
		var rand_new = floor(rand_range(min_number, max_number))
		while rand_new == rand:
			rand_new = floor(rand_range(min_number, max_number))
		var piece_1 = possible_pieces[rand].instance();
		var piece_2 = possible_pieces[rand_new].instance();
		add_child(piece_1);
		add_child(piece_2);
		# Set orientation
		if rand_new_piece_orientation == 0:
			new_piece_orientation = 1
			piece_1.position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height - 120 - 60);
			piece_2.position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height - 120 + 60);
		elif rand_new_piece_orientation == 1:
			new_piece_orientation = 2
			piece_1.position = Vector2((get_viewport_rect().size.x) / 2 - 60, spawn_ui_height - 120 );
			piece_2.position = Vector2((get_viewport_rect().size.x) / 2 + 60, spawn_ui_height - 120 );
		array.append([])
		array.append([])
		array[0] = piece_1
		array[1] = piece_2
	elif rand_new_piece_length == 1:
		# Spawn Single Tile
		new_piece_length = 1
		var piece = possible_pieces[rand].instance();
		add_child(piece);
		piece.position = Vector2((get_viewport_rect().size.x) / 2, spawn_ui_height- 120 );
		array = piece
	return array;

func grid_to_pixel(column, row):
	var new_x = x_start + offset * column;
	var new_y = y_start + -offset * row;
	return Vector2(new_x, new_y);

func pixel_to_grid(pixel_x, pixel_y):
	var new_x = round((pixel_x - x_start) / offset);
	var new_y = round((pixel_y - y_start) / -offset);
	return Vector2(new_x, new_y);
	pass;

func vector_to_grid(vector):
	var new_x = round((vector.x - x_start) / offset);
	var new_y = round((vector.y - y_start) / -offset);
	return Vector2(new_x, new_y);
	pass;

func dual_is_in_grid():
	# Return if both pieces from spawn array are inside the grid
	for i in spawn_piece_array.size():
		if vector_to_grid(spawn_piece_array[i].position).x >= 0 && vector_to_grid(spawn_piece_array[i].position).x < width && vector_to_grid(spawn_piece_array[i].position).y >= 0 && vector_to_grid(spawn_piece_array[i].position).y < height:
			pass
		else:
			return false
	return true

func is_in_grid(grid_position):
	if new_piece_length == 2:
		if dual_is_in_grid():
			#print("The dual dual_is_in_grid function returns true")
			#print(spawn_piece_array.size())
			for i in spawn_piece_array.size():
				#Determing if the array position of the pieces in that pos are null. may not be null because of queue_free
				#Got it working with is_instance_valid. https://github.com/godotengine/godot/issues/20549
				if not is_instance_valid(all_pieces[vector_to_grid(spawn_piece_array[i].position).x][vector_to_grid(spawn_piece_array[i].position).y]):
					pass
				else:
					return false
			return true
	else:
		if grid_position.x >= 0 && grid_position.x < width:
			if grid_position.y >= 0 && grid_position.y < height:
				return true;
		return false;

func single_is_in_grid(grid_position):
		if grid_position.x >= 0 && grid_position.x < width:
			if grid_position.y >= 0 && grid_position.y < height:
				return true;
		return false;

func find_matches(query = false, array = all_pieces):
	var match_detected = false
	for k in range(1, 8, 1):
		for i in width:
			for j in height:
				if is_instance_valid(array[i][j]):
					var current_number = array[i][j].number
					if current_number == k:
						# Detect horizontal Match
						if i > 0 && i < width - 1:
							if is_instance_valid(array[i-1][j]) && is_instance_valid(array[i+1][j]):
								if array[i - 1][j].number == current_number && array[i + 1][j].number == current_number:
									if query:
										return true
									array[i-1][j].matched = true
									array[i][j].matched = true
									array[i+1][j].matched = true
									match_detected = true
									# Reset scored Variable
									array[i][j].scored = false
						# Detect vertical Match
						if j > 0 && j < height - 1:
							if is_instance_valid(array[i][j-1]) && is_instance_valid(array[i][j + 1]):
								if array[i][j - 1].number == current_number && array[i][j + 1].number == current_number:
									if query:
										return true
									array[i][j - 1].matched = true
									array[i][j].matched = true
									array[i][j + 1].matched = true
									match_detected = true
									# Reset scored Variable
									array[i][j].scored = false
						# Detect └ Match
						if i < width - 1 && j < height - 1:
							if is_instance_valid(array[i+1][j]) && is_instance_valid(array[i][j + 1]):
								if array[i + 1][j].number == current_number && array[i][j+ 1].number == current_number:
										if query:
											return true
										array[i + 1][j].matched = true
										array[i][j].matched = true
										array[i][j + 1].matched = true
										match_detected = true
										# Reset scored Variable
										array[i][j].scored = false
						# Detect ┐ Match
						if i > 0 && j > 0:
							if is_instance_valid(array[i - 1][j]) && is_instance_valid(array[i][j - 1]):
								if array[i - 1][j].number == current_number && array[i][j - 1].number == current_number:
										if query:
											return true
										array[i - 1][j].matched = true
										array[i][j].matched = true
										array[i][j - 1].matched = true
										match_detected = true
										# Reset scored Variable
										array[i][j].scored = false
						# Detect ┌ Match
						if i < width - 1 && j > 0:
							if is_instance_valid(array[i + 1][j]) && is_instance_valid(array[i][j - 1]):
								if array[i + 1][j].number == current_number && array[i][j - 1].number == current_number:
										if query:
											return true
										array[i + 1][j].matched = true
										array[i][j].matched = true
										array[i][j - 1].matched = true
										match_detected = true
										# Reset scored Variable
										array[i][j].scored = false
						# Detect ┘ Match
						if i > 0 && j < height - 1:
							if is_instance_valid(array[i - 1][j]) && is_instance_valid(array[i][j + 1]):
								if array[i - 1][j].number == current_number && array[i][j + 1].number == current_number:
										if query:
											return true
										array[i - 1][j].matched = true
										array[i][j].matched = true
										array[i][j + 1].matched = true
										match_detected = true
										# Reset scored Variable
										array[i][j].scored = false
		if match_detected:
			return true
	if query:
		return false
	else: 
		return false

func destroy_matched():
	var root_position
	var root_coordinates

	for i in width:
		for j in height:
			if is_instance_valid(all_pieces[i][j]):
				if all_pieces[i][j].matched:
					# Update Score
					if not all_pieces[i][j].scored:
						score = int(score) + int(all_pieces[i][j].number)
						all_pieces[i][j].scored = true
					
					var current_number = all_pieces[i][j].number
					# Upgrade root piece to next number
					if all_pieces[i][j].matched_root:
						if current_number != 7:
							root_position = all_pieces[i][j]
							root_coordinates = root_position.position
							root_position.queue_free()
							var piece = possible_pieces[int(current_number)].instance()
							add_child(piece);
							piece.fadein()
							piece.position = grid_to_pixel(i, j);
							piece.matched_root = true
							all_pieces[i][j] = piece;
						elif current_number == 7:
							# Start 7 clean animation, based on location in grid
							make_effect(animated_clean, i, j)
							# Queue free all pieces in range of 7
							for k in range(-1,2):
								for l in range(-1,2):
									if single_is_in_grid(Vector2(i + k, j + l)):
										if is_instance_valid(all_pieces[i + k][j + l]):
											all_pieces[i + k][j + l].queue_free()
	var animated_piece
	for i in width:
		for j in height:
			if is_instance_valid(all_pieces[i][j]):
				if all_pieces[i][j].matched:
					if not all_pieces[i][j].matched_root && all_pieces[i][j].number != 7:
						#print(vector_to_grid(root_coordinates)))
						all_pieces[i][j].move_tween.connect("tween_completed", self, "_on_tween_completed")
						animated_piece = all_pieces[i][j]
						all_pieces[i][j].move(root_coordinates)
	if animated_piece != null:
		yield(animated_piece.get_node("move_tween"), "tween_completed")

func _on_tween_completed(object, key):
	#print("done, removing")
	object.queue_free()

func _on_destroy_timer_timeout():
	pass

var score_file = "user://score_save.txt"
var intro_played_file = "user://intro_played_file.txt"

func save_intro_played():
	var file = File.new()
	file.open(intro_played_file, File.WRITE)
	file.store_var(str(intro_played))
	file.close()

func load_intro_played():
	var file = File.new()
	if file.file_exists(intro_played_file):
		file.open(intro_played_file, File.READ)
		intro_played = file.get_var()
		file.close()
	else:
		intro_played = false

func save_score():
	var file = File.new()
	if int(score) >= int(highscore):
		file.open(score_file, File.WRITE)
		file.store_var(str(score))
		file.close()

func load_score():
	var file = File.new()
	if file.file_exists(score_file):
		file.open(score_file, File.READ)
		highscore = file.get_var()
		file.close()
	else:
		highscore = 0

func _on_intro_button_released():
	get_parent().get_node("intro").visible = false
	get_parent().get_node("intro/intro_button").visible = false
	get_parent().get_node("pause_button").visible = true
	get_parent().get_node("background_dark").visible = false
	intro_played = true
	save_intro_played()
	# Start the game on touch inputs when enable_game is true
	enable_game = true
	controlling = true

func _on_pause_button_released():
	if !get_parent().get_node("pause_menu").visible:
		get_parent().get_node("pause_menu").visible = true
		get_parent().get_node("pause_menu/resume_button").visible = true
		get_parent().get_node("pause_button").visible = false
		get_parent().get_node("background_dark").visible = true
		controlling = false
		enable_game = false
	else:
		get_parent().get_node("pause_menu").visible = false
		get_parent().get_node("pause_menu/resume_button").visible = false
		controlling = true
		enable_game = true

func _on_resume_pressed():
	get_parent().get_node("pause_menu").visible = false
	get_parent().get_node("pause_button").visible = true
	get_parent().get_node("background_dark").visible = false
	controlling = true
	enable_game = true

func _on_restart_pressed():
	get_tree().change_scene("res://scenes/game_window.tscn")

func _on_blog_released():
	OS.shell_open("https://blog.peterge.de/combined/")

func _on_donate_released():
	OS.shell_open("https://liberapay.com/peterge/")

func _on_gitlab_released():
	OS.shell_open("https://gitlab.com/peterge/combined")

func _on_resume_button_released():
	get_parent().get_node("pause_menu").visible = false
	get_parent().get_node("pause_button").visible = true
	get_parent().get_node("background_dark").visible = false
	controlling = true
	enable_game = true

func _on_intro_pressed():
	# Close Pause
	get_parent().get_node("pause_menu").visible = false
	get_parent().get_node("pause_button").visible = true
	get_parent().get_node("background_dark").visible = false
	get_parent().get_node("intro").visible = true
	# Show Intro
	get_parent().get_node("intro/intro_button").visible = true
	get_parent().get_node("pause_button").visible = false
	get_parent().get_node("background_dark").visible = true
	controlling = false
	enable_game = false
