extends CanvasLayer

func _ready():
	pass

func _on_Restart_pressed():
	get_tree().change_scene("res://scenes/game_window.tscn")

func _on_Quit_pressed():
	get_tree().quit()

func _on_blog_pressed():
	OS.shell_open("https://blog.peterge.de/combined/")

func _on_liberapay_pressed():
	OS.shell_open("https://liberapay.com/peterge/")

func _on_unpause_button_released():
	get_tree().change_scene("res://scenes/game_window.tscn")

func _on_gitlab_released():
	OS.shell_open("https://gitlab.com/peterge/combined")
